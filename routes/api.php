<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Requests $request) {
//    return $request->user();
//});

Route::get('/list', 'Controller@routeList');
Route::post('/login', 'Controller@login');
Route::post('/reset-password', 'Controller@resetPassword');
Route::post('/admin-login', 'Controller@loginAdmin');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/me', function (Request $request) {
        return \Illuminate\Support\Facades\Auth::user();
    });

    Route::middleware(['abilities:permission_access'])->group(function () {

    });
});


Route::middleware(['cors'])->group(function () {
    Route::resource('report', 'ReportController');

    Route::post('/report/remove/{id}', 'ReportController@destroy');

    Route::resource('task', 'TaskController');

    Route::get('/task/show/{id}', 'TaskController@show');

    Route::post('/task/edit/{id}', 'TaskController@edit');

    Route::post('/task/remove/{id}', 'TaskController@destroy');

    Route::resource('service-type', 'ServiceTypeController');

    Route::resource('service', 'ServiceController');

    Route::resource('construction', 'ConstructionController');

    Route::resource('user', 'UserController');

    Route::resource('role', 'RoleController');

    Route::resource('permission', 'PermissionController');

    Route::resource('status', 'ConstructionStatusController');

    Route::resource('city', 'CityController');
});


Route::middleware(['auth:sanctum','abilities:admin-all'])->group(function () {
    Route::get('/admin', function (Request $request) {
        return response()->json([
            'success' => true,
            'message' => 'Admin Login',
        ]);
    });

//    Route::resource('category', 'CategoryController');
});

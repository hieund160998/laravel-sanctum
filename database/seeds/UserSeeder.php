<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'omc.admin@gmail.com',
            'password' => \Carbon\Carbon::now(),
            'created_at' => Carbon\Carbon::now(),
            'username' => 'admin',
            'role_id' => 1,
            'status' => 1
        ]);
    }
}

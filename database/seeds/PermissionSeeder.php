<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'omc.admin@gmail.com',
            'password' => Hash::make("12345"),
            'created_at' => Carbon\Carbon::now(),
            'username' => 'admin',
            'tel' => '0978442111',
            'role_id' => 1,
            'status' => 1
        ]);

        DB::table('roles')->insert([
            'id' => 1,
            'title' => 'Admin',
            'description' => 'Master Account',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('permissions')->insert([
            'group_id' => 0,
            'group_name' => 'Super Admin',
            'title' => 'Super Admin',
            'key' => '*',
            'ability_key' => '*',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 1,
            'permission_id' => 1,
            'created_by' => 'admin'
        ]);

        foreach (Permission::PERMISSION_LIST as $group_id => $arr) {
            foreach ($arr as $ability_key => $title) {
                DB::table('permissions')->insert([
                    'group_id' => $group_id,
                    'group_name' => Permission::GROUP_NAME[$group_id],
                    'title' => $title,
                    'key' => $ability_key,
                    'ability_key' => $ability_key,
                    'created_at' => \Carbon\Carbon::now()
                ]);
            }
        }

    }
}

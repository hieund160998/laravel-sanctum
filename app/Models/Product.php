<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Psy\Util\Str;
use App\Models\Category;

class Product extends Model
{
    public $timestamps = true;
    protected $fillable = ['title', 'price', 'discount', 'summary', 'description', 'cat_id', 'child_cat_id', 'brand_id', 'status', 'photo'];
    protected $hidden = ['description', 'is_featured', 'cat_id', 'child_cat_id', 'brand_id'];

    public function filter($params): array
    {
        $page = !empty($params['page']) ? (int)$params['page'] : 1;
        $limit = !empty($params['limit']) ? (int)$params['limit'] : 10;
        $order = !empty($params['order']) ? $params['order'] : 'DESC';
        $sort = !empty($params['sort']) ? $params['sort'] : 'id';

        $query = $this->where('status', 'active');

        if (!empty($params['title'])) {
            $query = $query->where('title', '%' . $params['title'] . '%');
        }

        if (!empty($params['price_from']) && !empty($params['price_to'])) {
            $query = $query->whereBetween('price', [$params['price_from'], $params['price_to']]);
        }

        if (!empty($params['time_from']) && !empty($params['time_to'])) {
            $query = $query->whereBetween('created_at', [$params['time_from'], $params['time_to']]);
        }

        $count = $query->count();

        return [
            'data' => $query->orderBy($sort, $order)->forPage($page, $limit)->get(),
            'pagination' => [
                'total' => $count,
                'current_page' => $page,
                'limit' => $limit
            ]
        ];
    }

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'id', 'cat_id');
    }
}

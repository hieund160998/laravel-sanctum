<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class Role extends Model
{
    public $table = 'roles';
    public $timestamps = true;
    protected $fillable = ['title', 'description'];
    protected $hidden = ['deleted_at'];

    public function filter($params): array
    {
        $page = !empty($params['page']) ? (int)$params['page'] : 1;
        $limit = !empty($params['limit']) ? (int)$params['limit'] : 10;
        $order = !empty($params['order']) ? $params['order'] : 'DESC';
        $sort = !empty($params['sort']) ? $params['sort'] : 'id';

        $query = $this->where('status', '>=' ,1);

        if (!empty($params['title'])) {
            $query = $query->where('title', '%' . $params['title'] . '%');
        }

        if (!empty($params['time_from']) && !empty($params['time_to'])) {
            $query = $query->whereBetween('created_at', [$params['time_from'], $params['time_to']]);
        }

        $count = $query->count();

        return [
            'data' => $query->orderBy($sort, $order)->forPage($page, $limit)->get(),
            'pagination' => [
                'total' => $count,
                'current_page' => $page,
                'limit' => $limit
            ]
        ];
    }

    public function permissions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Permission::class,'permission_role','role_id','permission_id');
    }


    public function permissionRoles(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PermissionRole::class,'role_id','id');
    }

    public function createRole($params): Role
    {
        $role = new Role();
        $role->title = $params['title'] ?? '';
        $role->description = $params['description'] ?? '';
        $role->save();

        foreach ($params['permissions'] as $permission) {
            PermissionRole::insert([
                'role_id' => $role->id,
                'permission_id' => $permission,
                //TODO : change this
                'created_by' => 'admin',
                'created_at' => Carbon::now()
            ]);
        }

        $role['permissions'] = PermissionRole::where('role_id', $role->id)->get();

        return $role;
    }

    public function updateRole($params, $id): bool
    {
        $role = $this->find($id);

        if (empty($role)) return false;

        if (!empty($params['title'])) $role->title = $params['title'];
        if (!empty($params['description']))  $role->description = $params['description'];
        $role->save();

        PermissionRole::where('role_id', $role->id)->delete();

        foreach ($params['permissions'] as $permission) {
            PermissionRole::insert([
                'role_id' => $role->id,
                'permission_id' => $permission,
                //TODO : change this
                'created_by' => 'admin',
                'created_at' => Carbon::now()
            ]);
        }

        return true;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public $timestamps = true;
    protected $fillable = ['name', 'code', 'publish_day', 'publish_time', 'representative_name', 'address', 'test_day', 'construction_id', 'frequency', 'status',
        'hicon_comment', 'customer_comment', 'created_at', 'updated_at'];

    public function filter($params): array
    {
        $page = !empty($params['page']) ? (int)$params['page'] : 1;
        $limit = !empty($params['limit']) ? (int)$params['limit'] : 10;
        $order = !empty($params['order']) ? $params['order'] : 'DESC';
        $sort = !empty($params['sort']) ? $params['sort'] : 'id';

        $query = $this;

        if (!empty($params['name'])) {
            $query = $query->where('name', '%' . $params['name'] . '%');
        }

        if (!empty($params['code'])) {
            $query = $query->where('name', '%' . $params['name'] . '%');
        }

        if (!empty($params['construction_id'])) {
            $query = $query->where('construction_id', $params['construction_id']);
        }

        if (!empty($params['time_from']) && !empty($params['time_to'])) {
            $query = $query->whereBetween('created_at', [$params['time_from'], $params['time_to']]);
        }

        $count = $query->count();

        return [
            'data' => $query->with('task.reportTask')->with('item')->with('construction')->orderBy($sort, $order)->forPage($page, $limit)->get(),
            'pagination' => [
                'total' => $count,
                'current_page' => $page,
                'limit' => $limit
            ]
        ];
    }

    public function construction(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Construction::class, 'construction_id', 'id');
    }

    public function task(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Task::class, 'report_id','id');
    }

    public function item(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ReportItem::class, 'report_id', 'id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportTask extends Model
{
    public $timestamps = true;
    protected $fillable = ['report_id', 'task_id', 'photo', 'created_at', 'updated_at'];

    public function task(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->BelongsTo(ReportTask::class, 'report_id', 'id');
    }
}

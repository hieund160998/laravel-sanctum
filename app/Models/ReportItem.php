<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportItem extends Model
{
    public $timestamps = true;
    protected $fillable = ['report_id', 'item_name', 'item_unit', 'item_quantity', 'updated_at', 'created_at'];

}

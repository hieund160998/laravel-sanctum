<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConstructionStaff extends Model
{
    public $table = 'construction_staffs';
    public $timestamps = true;
    protected $fillable = ['user_id', 'construction_id'];
}

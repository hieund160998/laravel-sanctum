<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConstructionStatus extends Model
{
    protected $table = 'construction_status';
    protected $fillable = ['name', 'description'];
}

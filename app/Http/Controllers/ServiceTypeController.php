<?php

namespace App\Http\Controllers;

use App\Models\ServiceType;
use Illuminate\Http\JsonResponse;

class ServiceTypeController
{
    protected ServiceType $serviceType;

    public function __construct(ServiceType $serviceType)
    {
        $this->serviceType = $serviceType;
    }

    /**
     * @group  ServiceType Management
     * Display a list of ServiceTypes.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":1,"name":"B\u1ea3o tr\u00ec","is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-01-26T21:33:38.000000Z","updated_at":"2022-01-26T21:33:38.000000Z"},{"id":2,"name":"S\u1eeda ch\u1eefa","is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-01-26T21:33:38.000000Z","updated_at":"2022-01-26T21:33:38.000000Z"}]}
     */
    public function index()
    {
        //
        $res = $this->serviceType->all()->toArray();

        return response()->json(['success' => true,'data' => $res]);
    }

//    /**
//     * @group  ServiceType Management
//     * Save a new ServiceType to Database.
//     * @bodyParam  name string required ServiceType name.
//     * @bodyParam  type_id int required ServiceType type's id.
//     * @param Request $request
//     * @return JsonResponse
//     * @response {"success":true,"data":{"name":"Repair and Replace","type_id":"2","updated_at":"2022-01-11T06:31:00.000000Z","created_at":"2022-01-11T06:31:00.000000Z","id":2}}
//     */
//    public function store(Request $request)
//    {
//        // TODO: use validator to avoid mass assignment vulnerability
//        $res = $this->serviceType->create($request->all());
//
//        return response()->json([
//                'success' => true,
//                'data' => $res
//            ]
//        );
//    }
//
//    /**
//     * @group  ServiceType Management
//     * Display the specified ServiceType
//     *
//     * @urlParam  ServiceType required The ID of ServiceType.
//     * @param $id
//     * @return JsonResponse
//     * @response {"success":true,"data":{"id":1,"name":"Repost","type_id":1,"is_active":1,"created_by":"admin","updated_by":null,"created_at":"2022-01-11T04:22:16.000000Z","updated_at":"2022-01-11T04:22:16.000000Z","ServiceType_type":{"id":1,"name":"B\u1ea3o tr\u00ec","is_active":1,"created_by":"admin","updated_by":null,"created_at":"2022-01-11T14:18:48.000000Z","updated_at":"2022-01-11T14:18:50.000000Z"}}}
//     */
//    public function show($id): JsonResponse
//    {
//        //
//        $res = $this->serviceType->with('ServiceTypeType')->find($id);
//
//        return response()->json([
//                'success' => true,
//                'data' => $res
//            ]
//        );
//    }
//
//    /**
//     * @group  ServiceType Management
//     * Update the specified ServiceType in storage.
//     * @urlParam  ServiceType required The ID of updating ServiceType.
//     * @bodyParam  name string required ServiceType name.
//     * @bodyParam  type_id int required ServiceType type's id.
//     * @bodyParam  is_active int required enum(1,0)
//     * @param Request $request
//     * @param int $id
//     * @return JsonResponse
//     * @response {"success":true,"message":"Update successful"}
//     */
//    public function update(Request $request, $id): JsonResponse
//    {
//        $res = $this->serviceType->where('id', $id)->update($request->all());
//
//        return response()->json([
//            'success' => true,
//            'message' => "Update successful"
//        ]);
//    }
//
//    /**
//     * @group  ServiceType Management
//     * Remove the specified ServiceType from storage.
//     * @urlParam  ServiceType required The ID of removing ServiceType.
//     * @param int $id
//     * @return JsonResponse
//     * @response {"success":true,"message":"Delete successful"}
//     */
//    public function destroy($id): JsonResponse
//    {
//        //
//        $serviceType = $this->serviceType->destroy($id);
//
//        return response()->json([
//            'success' => true,
//            'message' => "Delete successful"
//        ]);
//    }
}

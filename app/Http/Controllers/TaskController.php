<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;

use App\Mail\ContactUsMail;
use App\Models\Task;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TaskController extends Controller
{
    protected Task $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * @group  Task Management
     * Display a listing of Tasks.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam name Search by Task title.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     *
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":1,"name":"T\u1ee7 trung t\u00e2m b\u00e1o ch\u00e1y","photo":"images\/QzJu1bF2ZLnyLBXcho8wli54KO3HCs9ntCJ42Tn7.jpg","description":"\u2022 Ki\u1ec3m tra ch\u1ee9c n\u0103ng t\u1ee7 trung t\u00e2m, m\u1ee9c \u0111\u1ed9 an to\u00e0n t\u01b0\u01a1ng th\u00edch c\u1ee7a h\u1ec7 th\u1ed1ng \r\n    \u2022 Ki\u1ec3m tra t\u00edn hi\u1ec7u th\u00f4ng s\u1ed1 k\u1ef9 thu\u1eadt bo m\u1ea1ch c\u1ee7a trung t\u00e2m b\u00e1o ch\u00e1y\r\n    \u2022 Ki\u1ec3m tra, \u0111o k\u1eb9p ch\u1ec9 s\u1ed1 A, V khi h\u1ec7 th\u1ed1ng ho\u1ea1t \u0111\u1ed9ng \r\n    \u2022 V\u1ec7 sinh t\u1ee7 trung t\u00e2m b\u00e1o ch\u00e1y","created_at":"2022-06-02T06:25:30.000000Z","updated_at":"2022-06-02T06:25:30.000000Z"}],"pagination":{"total":1,"current_page":1,"limit":10}}
     */
    public function index(Request $request)
    {
        //
        $res = $this->task->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }

    /**
     * @group  Task Management
     * Save a new Task to Database.
     * @bodyParam  name string required Task title.
     * @bodyParam  photo file Photo
     * @bodyParam  description Description
     * @return JsonResponse
     * @response {"success":true,"data":{"name":"T\u1ee7 trung t\u00e2m b\u00e1o ch\u00e1y","photo":"images\/QzJu1bF2ZLnyLBXcho8wli54KO3HCs9ntCJ42Tn7.jpg","description":"\u2022 Ki\u1ec3m tra ch\u1ee9c n\u0103ng t\u1ee7 trung t\u00e2m, m\u1ee9c \u0111\u1ed9 an to\u00e0n t\u01b0\u01a1ng th\u00edch c\u1ee7a h\u1ec7 th\u1ed1ng \r\n    \u2022 Ki\u1ec3m tra t\u00edn hi\u1ec7u th\u00f4ng s\u1ed1 k\u1ef9 thu\u1eadt bo m\u1ea1ch c\u1ee7a trung t\u00e2m b\u00e1o ch\u00e1y\r\n    \u2022 Ki\u1ec3m tra, \u0111o k\u1eb9p ch\u1ec9 s\u1ed1 A, V khi h\u1ec7 th\u1ed1ng ho\u1ea1t \u0111\u1ed9ng \r\n    \u2022 V\u1ec7 sinh t\u1ee7 trung t\u00e2m b\u00e1o ch\u00e1y","updated_at":"2022-06-02T06:25:30.000000Z","created_at":"2022-06-02T06:25:30.000000Z","id":1}}
     */
    public function store(Request $request)
    {
        // TODO: use validator to avoid mass assignment vulnerability
        if ($request->hasFile('photo')) {
          $path = $request->file('photo')->store('/images');
        }

        $task = $this->task->create([
            'name' => $request->input('name'),
            'photo' => $path ?? '',
            'description' => $request->input('description') ?? ''
        ]);

        return response()->json([
                'success' => true,
                'data' => $task
            ]
        );
    }

    /**
     * @group  Task Management
     * Display the specified resource.
     *
     * @urlParam  task required The ID of updating product.
     * @return JsonResponse
     * @response {"success":true,"data":{"id":1,"name":"T\u1ee7 trung t\u00e2m b\u00e1o ch\u00e1y","photo":"images\/QzJu1bF2ZLnyLBXcho8wli54KO3HCs9ntCJ42Tn7.jpg","description":"\u2022 Ki\u1ec3m tra ch\u1ee9c n\u0103ng t\u1ee7 trung t\u00e2m, m\u1ee9c \u0111\u1ed9 an to\u00e0n t\u01b0\u01a1ng th\u00edch c\u1ee7a h\u1ec7 th\u1ed1ng \r\n    \u2022 Ki\u1ec3m tra t\u00edn hi\u1ec7u th\u00f4ng s\u1ed1 k\u1ef9 thu\u1eadt bo m\u1ea1ch c\u1ee7a trung t\u00e2m b\u00e1o ch\u00e1y\r\n    \u2022 Ki\u1ec3m tra, \u0111o k\u1eb9p ch\u1ec9 s\u1ed1 A, V khi h\u1ec7 th\u1ed1ng ho\u1ea1t \u0111\u1ed9ng \r\n    \u2022 V\u1ec7 sinh t\u1ee7 trung t\u00e2m b\u00e1o ch\u00e1y","created_at":"2022-06-02T06:25:30.000000Z","updated_at":"2022-06-02T06:25:30.000000Z"}}
    */
    public function show($id)
    {
        //
        $dataArray = [
            'full_name' => "Hieu",
            'subject' => "Password for your account",
            'phone' => "0989281",
            'email' => "hieundse05675@fpt.edu.vn",
            'message' =>"here you password: 19082387"
        ];

        $sendToEmail = strtolower('hieundse05675@fpt.edu.vn');
        Mail::to($sendToEmail)->send(new ContactUsMail($dataArray));


        //$res = $this->task->find($id);

        return response()->json([
                'success' => true,
                'data' => 123
            ]
        );
    }

    /**
     * @group  Task Management
     * Update the specified Task in storage.
     *
     * @urlParam  task required The ID of updating task.
     * @bodyParam  name string required Task title.
     * @bodyParam  photo file Photo
     * @bodyParam  description Description
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Update successful"}
     */
    public function edit(Request $request, $id)
    {
        $task = $this->task->find($id);

        if ($request->hasFile('photo')) {
            $path = $request->file('photo')->store('/images');
            @unlink(storage_path('app/public/'. $task->photo));
        }

        $res = $this->task->where('id', $id)->update([
            'name' => $request->input('name') ?? $task->name,
            'photo' => $path ?? $task->photo,
            'description' => $request->input('description') ??  $task->description
        ]);

        return response()->json([
            'success' => true,
            'message' => "Update successful"
        ]);
    }

    /**
     * @group  Task Management
     * Remove the specified resource from storage.
     *
     * @urlParam  task required The ID of removing task.
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Delete successful"}
     */
    public function destroy($id)
    {
        $task = $this->task->find($id);
        $this->task->destroy($id);
        @unlink(storage_path('app/public/'. $task->photo));

        return response()->json([
            'success' => true,
            'message' => "Delete successful"
        ]);
    }
}


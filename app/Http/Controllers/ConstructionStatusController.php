<?php

namespace App\Http\Controllers;

use App\Models\ConstructionStatus;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class ConstructionStatusController extends Controller
{
    protected ConstructionStatus $constructionStatus;

    public function __construct(ConstructionStatus $constructionStatus)
    {
        $this->constructionStatus = $constructionStatus;
    }

    /**
     * @group  ConstructionStatus Management
     * Display a list of Construction Status.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":1,"name":"Ho\u00e0n th\u00e0nh","description":"Done","created_at":"2022-04-18T22:54:05.000000Z","updated_at":null},{"id":2,"name":"Dang thi c\u00f4ng","description":"Doing","created_at":"2022-04-18T15:50:13.000000Z","updated_at":"2022-04-18T15:50:13.000000Z"}]}
     */
    public function index()
    {
        //
        $res = $this->constructionStatus->orderBy('created_at', 'DESC')->get()->toArray();

        return response()->json(['success' => true, 'data' => $res]);
    }


    /**
     * @group  ConstructionStatus Management
     * Save a new Construction Status to Database.
     * @bodyParam  name string required Status name.
     * @bodyParam  description string Status description.
     * @param Request $request
     * @return JsonResponse
     * @response {"success":true,"data":{"name":"Ho\u00e0n th\u00e0nh","description":"ho\u00e0n th\u00e0nh","updated_at":"2022-04-18T15:50:13.000000Z","created_at":"2022-04-18T15:50:13.000000Z","id":2}}
     */
    public function store(Request $request): JsonResponse
    {
        // TODO: use validator to avoid mass assignment vulnerability
        $res = $this->constructionStatus->create($request->all());

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

}

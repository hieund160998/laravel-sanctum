<?php

namespace App\Http\Controllers;

use App\Models\Construction;
use App\Models\ConstructionStaff;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConstructionController
{
    protected Construction $construction;
    protected ConstructionStaff  $constructionStaff;

    public function __construct(Construction $construction, ConstructionStaff $constructionStaff)
    {
        $this->constructionStaff = $constructionStaff;
        $this->construction = $construction;
    }

    /**
     * @group  Construction Management
     * Display a list of Constructions.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam name Search by Construction name.
     * @queryParam city_id Search by City.
     * @queryParam status Search by Construction status.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     * @queryParam service_id Search by Construction name.
     * @queryParam service_type_id Search by Construction name.
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":9,"name":"YUTO","period":null,"address":"L\u00f4 K13 khu c\u00f4ng nghi\u1ec7p Qu\u1ebf V\u00f5, B\u1eafc Ninh","service_id":16,"service_type_id":1,"status":{"id":1,"name":"M\u1edbi t\u1ea1o","description":"M\u1edbi t\u1ea1o","created_at":null,"updated_at":null},"person_in_charge":"B\u00f9i Thanh B\u00ecnh","representative_id":null,"city_id":69,"representative":"Ph\u1ea1m \u0110\u00fac H\u1ea3i","representative_tel":"0968095072","representative_mail":"hai.phamduc@hiconmne.vn","created_by":null,"updated_by":null,"created_at":"2022-04-19T20:05:45.000000Z","updated_at":"2022-04-19T20:05:45.000000Z","staffs":[],"city":{"id":69,"name":"B\u1eafc Ninh","description":"B\u1eafc Ninh","created_at":null,"updated_at":null},"service":{"id":16,"name":"C\u1ea3i t\u1ea1o nh\u00e0 x\u01b0\u1edfng","type_id":1,"is_active":1,"created_by":null,"updated_by":null,"created_at":"2022-04-19T20:03:56.000000Z","updated_at":"2022-04-19T20:03:56.000000Z"},"service_type":{"id":1,"name":"B\u1ea3o tr\u00ec","is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-01-27T09:33:38.000000Z","updated_at":"2022-01-27T09:33:38.000000Z"}}],"pagination":{"total":1,"current_page":1,"limit":10}}
     */
    public function index(Request $request)
    {
        //
        $res = $this->construction->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }

    /**
     * @group  Construction Management
     * Save a new Construction to Database.
     * @bodyParam  name string required Construction name.
     * @bodyParam  period string Construction period (ky bao tri).
     * @bodyParam  address string required Construction address.
     * @bodyParam  service_id int required Service's id.
     * @bodyParam  service_type_id int required Service type's id.
     * @bodyParam  city_id int City's id.
     * @bodyParam  status int Construction's status.
     * @bodyParam  staffs array Array id cac' nhân viên phụ trách.
     * @bodyParam  representative string required Representative's name.
     * @bodyParam  representative_tel string required Representative's tel.
     * @bodyParam  representative_mail string required Representative's mail.
     * @param Request $request
     * @return JsonResponse
     * @response {"success":true,"data":{"name":"B\u1ea3o tr\u00ec thang m\u00e1y","service_type_id":"1","address":"124 Th\u1ed5 Quan, \u0110\u1ed1ng \u0110a , HN","service_id":"2","person_in_charge":"Mai Thi L\u1ea1i","representative":"Tr\u1ecbnh V\u0103n Quy\u1ebft","representative_mail":"flc@gmail.com","representative_tel":"+84144000222","updated_at":"2022-01-27T07:55:32.000000Z","created_at":"2022-01-27T07:55:32.000000Z","id":4}}
     */
    public function store(Request $request): JsonResponse
    {
        // TODO: use validator to avoid mass assignment vulnerability

        try {
            DB::beginTransaction();
            $res = $this->construction->create([
                'name' => $request->input('name'),
                'period' => $request->input('period'),
                'address' => $request->input('address'),
                'service_id' => $request->input('service_id'),
                'service_type_id' => $request->input('service_type_id'),
                'city_id' => $request->input('city_id'),
                'status' => $request->input('status'),
                'representative_id' => $request->input('representative_id'),
                'person_in_charge' => 'omc',
                'representative' =>  $request->input('representative'),
                'representative_tel' =>  $request->input('representative_tel'),
                'representative_mail' =>  $request->input('representative_mail'),
            ]);

            foreach ($request->input('staffs') as $staffID) {
                $this->constructionStaff->create([
                    'construction_id' => $res->id,
                    'user_id' => $staffID,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ]
            );
        }

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Construction Management
     * Display the specified Construction
     *
     * @urlParam  construction required The ID of Construction.
     * @param $id
     * @return JsonResponse
     * @response {"success":true,"data":{"id":9,"name":"YUTO","period":null,"address":"L\u00f4 K13 khu c\u00f4ng nghi\u1ec7p Qu\u1ebf V\u00f5, B\u1eafc Ninh","service_id":16,"service_type_id":1,"status":1,"person_in_charge":"B\u00f9i Thanh B\u00ecnh","representative_id":null,"city_id":69,"representative":"Ph\u1ea1m \u0110\u00fac H\u1ea3i","representative_tel":"0968095072","representative_mail":"hai.phamduc@hiconmne.vn","created_by":null,"updated_by":null,"created_at":"2022-04-19T20:05:45.000000Z","updated_at":"2022-04-19T20:05:45.000000Z","staffs":[],"service":{"id":16,"name":"C\u1ea3i t\u1ea1o nh\u00e0 x\u01b0\u1edfng","type_id":1,"is_active":1,"created_by":null,"updated_by":null,"created_at":"2022-04-19T20:03:56.000000Z","updated_at":"2022-04-19T20:03:56.000000Z"},"service_type":{"id":1,"name":"B\u1ea3o tr\u00ec","is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-01-27T09:33:38.000000Z","updated_at":"2022-01-27T09:33:38.000000Z"}}}
     */
    public function show($id): JsonResponse
    {
        //
        $res = $this->construction->with('staffs')->with('service')->with('serviceType')->find($id);

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Construction Management
     * Update the specified Construction in storage.
     * @urlParam  service required The ID of updating Construction.
     * @bodyParam  name string Construction name.
     * @bodyParam  period string Construction period (ky bao tri).
     * @bodyParam  address string Construction address.
     * @bodyParam  service_id int Service's id.
     * @bodyParam  city_id int City's id.
     * @bodyParam  status int Construction's status.
     * @bodyParam  staffs array Array id cac' nhân viên phụ trách.
     * @bodyParam  representative string required Representative's name.
     * @bodyParam  representative_tel string required Representative's tel.
     * @bodyParam  representative_mail string required Representative's mail.
     * @bodyParam  service_type_id int Service type's id.
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Update successful"}
     */
    public function update(Request $request, $id): JsonResponse
    {
        $staffs = $request->input('staffs');
        $request->request->remove('staffs');
        try {
            $res = $this->construction->where('id', $id)->update($request->all());

            $this->constructionStaff->where('construction_id', $id)->delete();

            foreach ($staffs as $staffID) {
                $this->constructionStaff->create([
                    'construction_id' => $id,
                    'user_id' => $staffID,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ]
            );
        }


        return response()->json([
            'success' => true,
            'message' => "Update successful"
        ]);
    }

    /**
     * @group  Construction Management
     * Remove the specified Construction from storage.
     * @urlParam  service required The ID of removing Construction.
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Delete successful"}
     */
    public function destroy($id): JsonResponse
    {
        //
        $Construction = $this->construction->destroy($id);

        return response()->json([
            'success' => true,
            'message' => "Delete successful"
        ]);
    }
}

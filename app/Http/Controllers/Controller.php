<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsMail;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @group  Auth Management
     * Update the specified Role in storage.
     * @bodyParam  username string required username.
     * @bodyParam  password string required password
     * @param Request $request
     * @return JsonResponse
     * @response {"status_code":200,"access_token":"2|7gV677eVm7oa4zn6t9q59JFjtQpQ5tpmx5LXKJ89","permissions":[2,3,4],"abilities":["user-create","password-reset","user-update"],"token_type":"Bearer"}
     */
    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $request->validate([
                'username' => 'required',
                'password' => 'required'
            ]);

            $user = User::where('username', '=', $request->username)->orWhere('email', '=', $request->username)->first();

            if (empty($user)) throw new \Exception('username not found');

            if (!Hash::check($request->password, $user->password)) throw new \Exception('Invalid password.');

            $permissionIds = PermissionRole::where('role_id', $user->role_id)->pluck('permission_id');
            $ablities = Permission::whereIn('id', $permissionIds)->pluck('ability_key');

            $tokenResult = $user->createToken('authToken', $ablities->toArray())->plainTextToken;

            return response()->json([
                'status_code' => 200,
                'access_token' => $tokenResult,
                'permissions' => $permissionIds,
                'abilities' => $ablities,
                'token_type' => 'Bearer',
                'user_info' => $user
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in Login',
                'error' => $error,
            ]);
        }
    }


    /**
     * @group  Auth Management
     * Reset password
     * @bodyParam  username string required username.
     * @bodyParam  email string required email
     * @param Request $request
     * @return JsonResponse
     * @response {"success":true,"message":"Mật khẩu mới đã được gửi tới hòm thư của bạn !"}
     */
    public function resetPassword(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $request->validate([
                'username' => 'required',
                'email' => 'email|required'
            ]);

            $user = User::where('username', '=', $request->username)->where('email', '=', $request->email)->first();

            if (empty($user)) throw new \Exception('User not found.');

            $newPassword = Str::random(8);
            $hashed_random_password = Hash::make($newPassword);

            $user->password = $hashed_random_password;
            $user->save();

            $dataArray = [
                'full_name' => $user->name,
                'subject' => "Reset mật khẩu TK OMCloud",
                'phone' => $user->tel,
                'email' => $user->email,
                'message' => $newPassword
            ];

            $sendToEmail = strtolower($user->email);
            Mail::to($sendToEmail)->send(new ContactUsMail($dataArray));

            return response()->json([
                'success' => true,
                'message' => 'Mật khẩu mới đã được gửi tới hòm thư của bạn !'
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'message' => 'Error in Reset Password ' . $error->getMessage(),
                'success' => false,
            ]);
        }
    }

    public function routeList(){
        $controllers = [];

        foreach (Route::getRoutes()->getRoutes() as $route)
        {
            $action = $route->getAction();

            if (array_key_exists('controller', $action))
            {
                // You can also use explode('@', $action['controller']); here
                // to separate the class name from the method
                if (strpos($action['controller'], '@') !== false) {
                    [$key, $value] = explode('@', $action['controller']);
                    $key = last(explode("\\", $key));
                    $controllers[$key][] = $value;
                }
            }
        }

        return response()->json($controllers);
    }

    public function loginAdmin(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);

            $user = User::where('email', $request->email)->where('role', 'admin')->first();

            if (empty($user)) throw new \Exception('email not found');

            $hashPass = Hash::make($request->password);
            if (Hash::check($hashPass, $user->password)) throw new \Exception('Invalid password.');

            $tokenResult = $user->createToken('authToken', ['admin-all'])->plainTextToken;

            return response()->json([
                'status_code' => 200,
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in Login',
                'error' => $error,
            ]);
        }
    }
}

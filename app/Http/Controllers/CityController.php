<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CityController
{
    protected City $city;

    public function __construct(City $city)
    {
        $this->city = $city;
    }

    /**
     * @group  City Management
     * Display a list of cities.
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam name Search by user's name.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":91,"name":"H\u1eadu Giang","description":"H\u1eadu Giang","created_at":null,"updated_at":null},{"id":90,"name":"H\u1ea3i Ph\u00f2ng","description":"H\u1ea3i Ph\u00f2ng","created_at":null,"updated_at":null},{"id":89,"name":"H\u1ea3i D\u01b0\u01a1ng","description":"H\u1ea3i D\u01b0\u01a1ng","created_at":null,"updated_at":null},{"id":88,"name":"H\u00e0 T\u0129nh","description":"H\u00e0 T\u0129nh","created_at":null,"updated_at":null},{"id":87,"name":"H\u00e0 N\u1ed9i","description":"H\u00e0 N\u1ed9i","created_at":null,"updated_at":null},{"id":86,"name":"H\u00e0 Nam","description":"H\u00e0 Nam","created_at":null,"updated_at":null},{"id":85,"name":"H\u00e0 Giang","description":"H\u00e0 Giang","created_at":null,"updated_at":null}],"pagination":{"total":7,"current_page":1,"limit":10}}
     */
    public function index(Request $request)
    {
        $res = $this->city->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }

}

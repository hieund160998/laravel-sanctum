<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\ReportItem;
use App\Models\ReportTask;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ReportController extends Controller
{
    protected Report $report;
    protected Task $task;
    protected ReportTask $reportTask;
    protected ReportItem  $reportItem;

    public function __construct(Report $report, Task $task, ReportTask $reportTask, ReportItem $reportItem)
    {
        $this->report = $report;
        $this->task = $task;
        $this->reportTask = $reportTask;
        $this->reportItem = $reportItem;
    }

    /**
     * @group  Report Management
     * Display a listing of Reports.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam construction_id Search by construction_id.
     * @queryParam name Search by Report title.
     * @queryParam code Search by Report code.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     *
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":35,"name":"test","code":"323eaAAF","publish_day":"12\/12\/2022","publish_time":null,"representative_name":"Van Thanh","address":"tes tsets","construction_id":4,"frequency":1,"valid_date":null,"hicon_comment":"test","customer_comment":"test","created_at":"2022-06-09T09:26:12.000000Z","updated_at":"2022-06-09T09:26:12.000000Z","task":[{"id":27,"name":"Tu chua chay","photo":"","description":"test","created_at":"2022-06-09T09:26:12.000000Z","updated_at":"2022-06-09T09:26:12.000000Z","report_id":35,"report_task":[{"id":23,"report_id":35,"task_id":27,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","photo":"images\/8l2GoiXnYMAOuaZZTJo2JZgwHiyrlqr2arc0aEL5.jpg"},{"id":24,"report_id":35,"task_id":27,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","photo":"images\/RqrExtVZp5kZloZvgURhJNxU1VioFXYAZd3KhHH4.jpg"}]},{"id":28,"name":"Cap dong","photo":"","description":"test","created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","report_id":35,"report_task":[{"id":25,"report_id":35,"task_id":28,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","photo":"images\/rC19FjptvyAR9Er8fysGPs82kfqCYFc35eizxPdW.jpg"},{"id":26,"report_id":35,"task_id":28,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","photo":"images\/FtusUc1meK0eUDtA7RjbWj9oCmcTgJ54EQI3EDAQ.jpg"}]}],"item":[{"id":4,"report_id":35,"item_name":"thiet bi","item_unit":"cai","item_quantity":2,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z"}],"construction":null},{"id":34,"name":"test","code":"323eaAAF","publish_day":"12\/12\/2022","publish_time":null,"representative_name":"Van Thanh","address":"tes tsets","construction_id":4,"frequency":1,"valid_date":null,"hicon_comment":"test","customer_comment":"test","created_at":"2022-06-09T09:16:32.000000Z","updated_at":"2022-06-09T09:16:32.000000Z","task":[{"id":25,"name":"Tu chua chay","photo":"","description":"test","created_at":"2022-06-09T09:16:32.000000Z","updated_at":"2022-06-09T09:16:32.000000Z","report_id":34,"report_task":[{"id":19,"report_id":34,"task_id":25,"created_at":"2022-06-09T09:16:32.000000Z","updated_at":"2022-06-09T09:16:32.000000Z","photo":"images\/fxZLRpmWzsy6pRpYcIs1Z1CENWJNsNw8ZIFymc7U.jpg"},{"id":20,"report_id":34,"task_id":25,"created_at":"2022-06-09T09:16:32.000000Z","updated_at":"2022-06-09T09:16:32.000000Z","photo":"images\/54li8jLk9r1YphKHeRaxzIKYaQLgfbtehB72MCdJ.jpg"}]},{"id":26,"name":"Cap dong","photo":"","description":"test","created_at":"2022-06-09T09:16:32.000000Z","updated_at":"2022-06-09T09:16:32.000000Z","report_id":34,"report_task":[{"id":21,"report_id":34,"task_id":26,"created_at":"2022-06-09T09:16:32.000000Z","updated_at":"2022-06-09T09:16:32.000000Z","photo":"images\/4G33sEMT5BMveyoMyvPet0IJoPwjBUr0Hqgy2qcX.jpg"},{"id":22,"report_id":34,"task_id":26,"created_at":"2022-06-09T09:16:32.000000Z","updated_at":"2022-06-09T09:16:32.000000Z","photo":"images\/rBwdSW4IMRKddaKLtza4Hj9wRuwGUsO2xHMGqp6R.jpg"}]}],"item":[{"id":3,"report_id":34,"item_name":"thiet bi","item_unit":"cai","item_quantity":2,"created_at":"2022-06-09T09:16:32.000000Z","updated_at":"2022-06-09T09:16:32.000000Z"}],"construction":null}],"pagination":{"total":2,"current_page":1,"limit":10}}
     */
    public function index(Request $request)
    {
        //
        $res = $this->report->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }

    /**
     * @group  Report Management
     * Display the specified resource.
     *
     * @urlParam  task required The ID of updating product.
     * @return JsonResponse
     * @response {"success":true,"data":{"id":35,"name":"test","code":"323eaAAF","publish_day":"12\/12\/2022","publish_time":null,"representative_name":"Van Thanh","address":"tes tsets","construction_id":4,"frequency":1,"valid_date":null,"hicon_comment":"test","customer_comment":"test","created_at":"2022-06-09T09:26:12.000000Z","updated_at":"2022-06-09T09:26:12.000000Z","task":[{"id":27,"name":"Tu chua chay","photo":"","description":"test","created_at":"2022-06-09T09:26:12.000000Z","updated_at":"2022-06-09T09:26:12.000000Z","report_id":35,"report_task":[{"id":23,"report_id":35,"task_id":27,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","photo":"images\/8l2GoiXnYMAOuaZZTJo2JZgwHiyrlqr2arc0aEL5.jpg"},{"id":24,"report_id":35,"task_id":27,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","photo":"images\/RqrExtVZp5kZloZvgURhJNxU1VioFXYAZd3KhHH4.jpg"}]},{"id":28,"name":"Cap dong","photo":"","description":"test","created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","report_id":35,"report_task":[{"id":25,"report_id":35,"task_id":28,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","photo":"images\/rC19FjptvyAR9Er8fysGPs82kfqCYFc35eizxPdW.jpg"},{"id":26,"report_id":35,"task_id":28,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z","photo":"images\/FtusUc1meK0eUDtA7RjbWj9oCmcTgJ54EQI3EDAQ.jpg"}]}],"item":[{"id":4,"report_id":35,"item_name":"thiet bi","item_unit":"cai","item_quantity":2,"created_at":"2022-06-09T09:26:13.000000Z","updated_at":"2022-06-09T09:26:13.000000Z"}],"construction":null}}
     */
    public function show($id)
    {
        $res = $this->report->where('id', $id)->with('task.reportTask')->with('item')->with('construction')->first();

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }


    /**
     * @group  Report Management
     * Save a new Report to Database.
     * @bodyParam  name string required Tiêu đề báo cáo.
     * @bodyParam  code string required Mã ban hành.
     * @bodyParam  publish_day string required Ngày ban hành.
     * @bodyParam  publish_time string required Lần ban hành.
     * @bodyParam  representative_name string required Họ tên đại diện.
     * @bodyParam  address string required Địa chỉ.
     * @bodyParam  construction_id int required ID dự án (công trình).
     * @bodyParam  frequency int required Tần suất bảo dưỡng ( 1 : Hàng tháng, 2: Hàng quý, 3: Nửa năm, 4: hằng năm).
     * @bodyParam  valid_date string required Ngày kiểm tra
     * @bodyParam  tasks[i][name] string Tên công việc
     * @bodyParam  tasks[i][images][j] file Ảnh chụp
     * @bodyParam  tasks[i][description] string required Mô tả công việc
     * @bodyParam  items[i][name] string required Tên thiết bị
     * @bodyParam  items[i][unit] string required Đơn vị
     * @bodyParam  items[i][quantity] int required Số lượng thiết bị
     * @bodyParam  hicon_comment string required Nhận xét của Hicon
     * @bodyParam  customer_comment string required Nhận xét của khách hàng
     * @bodyParam  status int required Trạng thái báo cáo
     * @request {"success":true,"data":{"name":"B\u00c1OC\u00c1OB\u1ea2OTR\u00ccH\u1ec6TH\u1ed0NGPH\u00d2NGCH\u00c1YCH\u1eeeACH\u00c1Y","code":"KSBT-BC-01","publish_day":"05-01-2016","publish_time":"01","representative_name":"Khuv\u1ef1cs\u1ea3nxu\u1ea5tS\u00f4ngH\u1ed3ng3","address":"HaiHau,Nam\u0110\u1ecbnh","construction_id":1,"frequency":1,"hicon_comment":null,"customer_comment":null,"updated_at":"2022-06-03T07:55:03.000000Z","created_at":"2022-06-03T07:55:03.000000Z","id":9}}
     * @return JsonResponse
     * @response {"success":true,"data":{"name":"B\u00c1O C\u00c1O B\u1ea2O TR\u00cc H\u1ec6 TH\u1ed0NG PH\u00d2NG CH\u00c1Y CH\u1eeeA CH\u00c1Y","code":"KSBT-BC-01","publish_day":"05-01-2016","publish_time":"01","representative_name":"Khu v\u1ef1c s\u1ea3n xu\u1ea5t S\u00f4ng H\u1ed3ng 3","address":"Hai Hau, Nam \u0110\u1ecbnh","construction_id":1,"frequency":1,"hicon_comment":null,"customer_comment":null,"updated_at":"2022-06-03T07:55:03.000000Z","created_at":"2022-06-03T07:55:03.000000Z","id":9}}
     */
    public function store(Request $request)
    {
        // TODO: use validator to avoid mass assignment vulnerability

        try {
            DB::beginTransaction();
            $files = $request->files->all();

            $report = $this->report->create([
                'name' => $request->input('name'),
                'code' => $request->input('code'),
                'publish_day' => $request->input('publish_day'),
                'publish_time' => $request->input('publish_time'),
                'representative_name' => $request->input('representative_name'),
                'address' => $request->input('address'),
                'construction_id' => $request->input('construction_id'),
                'frequency' => $request->input('frequency'),
                'valid_date' => $request->input('valid_date'),
                'hicon_comment' => $request->input('hicon_comment'),
                'customer_comment' => $request->input('customer_comment'),
                'status' => $request->input('status'),
            ]);

            $tasks = $request->input('tasks');
            foreach ($tasks as $key => $task) {
                $savedTask = $this->task->create([
                    'report_id' => $report->id,
                    'name' => $task['name'],
                    'description' => $task['description'],
                    'photo' => ""
                ]);

                $taskImages = $files['tasks'][$key]['images'] ?? [];

                foreach ($taskImages as $keyImg => $image) {
                    //$path = $image->store('/images');
                    $path = $request->file('tasks')[$key]['images'][$keyImg]->store('/images');

                    $this->reportTask->create([
                        'report_id' => $report->id,
                        'task_id' => $savedTask->id,
                        'photo' => $path
                    ]);
                }
            }

            $rpItems = $request->input('items');

            foreach ($rpItems as $item) {
                $this->reportItem->create([
                    'report_id' => $report->id,
                    'item_name' => $item['name'],
                    'item_unit' => $item['unit'],
                    'item_quantity' => $item['quantity']
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ]
            );
        }

        return response()->json([
                'success' => true,
                'data' => $report
            ]
        );
    }

    /**
     * @group  Report Management
     * Remove the specified resource from storage.
     *
     * @urlParam  report required The ID of removing report.
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Delete successful"}
     */
    public function destroy($id)
    {
        $task = $this->report->find($id);
        $this->report->destroy($id);

        return response()->json([
            'success' => true,
            'message' => "Delete successful"
        ]);
    }

}

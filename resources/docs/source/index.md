---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Auth Management


<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Update the specified Role in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"username":"et","password":"quis"}'

```

```javascript
const url = new URL(
    "http://localhost/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "username": "et",
    "password": "quis"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status_code": 200,
    "access_token": "2|7gV677eVm7oa4zn6t9q59JFjtQpQ5tpmx5LXKJ89",
    "permissions": [
        2,
        3,
        4
    ],
    "abilities": [
        "user-create",
        "password-reset",
        "user-update"
    ],
    "token_type": "Bearer"
}
```

### HTTP Request
`POST api/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  required  | username.
        `password` | string |  required  | password
    
<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_6d3061d375666b8cf6babe163b36f250 -->
## Reset password

> Example request:

```bash
curl -X POST \
    "http://localhost/api/reset-password" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"username":"nulla","email":"animi"}'

```

```javascript
const url = new URL(
    "http://localhost/api/reset-password"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "username": "nulla",
    "email": "animi"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Mật khẩu mới đã được gửi tới hòm thư của bạn !"
}
```

### HTTP Request
`POST api/reset-password`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  required  | username.
        `email` | string |  required  | email
    
<!-- END_6d3061d375666b8cf6babe163b36f250 -->

#City Management


<!-- START_3ec0c0efa109ce210908d51cbecdb998 -->
## Display a list of cities.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/city?sort=voluptatem&page=17&limit=5&order=omnis&name=rerum&time_from=quibusdam&time_to=quasi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/city"
);

let params = {
    "sort": "voluptatem",
    "page": "17",
    "limit": "5",
    "order": "omnis",
    "name": "rerum",
    "time_from": "quibusdam",
    "time_to": "quasi",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 91,
            "name": "Hậu Giang",
            "description": "Hậu Giang",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 90,
            "name": "Hải Phòng",
            "description": "Hải Phòng",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 89,
            "name": "Hải Dương",
            "description": "Hải Dương",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 88,
            "name": "Hà Tĩnh",
            "description": "Hà Tĩnh",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 87,
            "name": "Hà Nội",
            "description": "Hà Nội",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 86,
            "name": "Hà Nam",
            "description": "Hà Nam",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 85,
            "name": "Hà Giang",
            "description": "Hà Giang",
            "created_at": null,
            "updated_at": null
        }
    ],
    "pagination": {
        "total": 7,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/city`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `name` |  optional  | Search by user's name.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss

<!-- END_3ec0c0efa109ce210908d51cbecdb998 -->

#Construction Management


<!-- START_92ef7348c3d91e762dad2d149050df98 -->
## Display a list of Constructions.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/construction?sort=labore&page=3&limit=16&order=architecto&name=vitae&city_id=aut&status=nobis&time_from=doloremque&time_to=corporis&service_id=labore&service_type_id=rem" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/construction"
);

let params = {
    "sort": "labore",
    "page": "3",
    "limit": "16",
    "order": "architecto",
    "name": "vitae",
    "city_id": "aut",
    "status": "nobis",
    "time_from": "doloremque",
    "time_to": "corporis",
    "service_id": "labore",
    "service_type_id": "rem",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 9,
            "name": "YUTO",
            "period": null,
            "address": "Lô K13 khu công nghiệp Quế Võ, Bắc Ninh",
            "service_id": 16,
            "service_type_id": 1,
            "status": {
                "id": 1,
                "name": "Mới tạo",
                "description": "Mới tạo",
                "created_at": null,
                "updated_at": null
            },
            "person_in_charge": "Bùi Thanh Bình",
            "representative_id": null,
            "city_id": 69,
            "representative": "Phạm Đúc Hải",
            "representative_tel": "0968095072",
            "representative_mail": "hai.phamduc@hiconmne.vn",
            "created_by": null,
            "updated_by": null,
            "created_at": "2022-04-19T20:05:45.000000Z",
            "updated_at": "2022-04-19T20:05:45.000000Z",
            "staffs": [],
            "city": {
                "id": 69,
                "name": "Bắc Ninh",
                "description": "Bắc Ninh",
                "created_at": null,
                "updated_at": null
            },
            "service": {
                "id": 16,
                "name": "Cải tạo nhà xưởng",
                "type_id": 1,
                "is_active": 1,
                "created_by": null,
                "updated_by": null,
                "created_at": "2022-04-19T20:03:56.000000Z",
                "updated_at": "2022-04-19T20:03:56.000000Z"
            },
            "service_type": {
                "id": 1,
                "name": "Bảo trì",
                "is_active": 1,
                "created_by": "admin",
                "updated_by": "admin",
                "created_at": "2022-01-27T09:33:38.000000Z",
                "updated_at": "2022-01-27T09:33:38.000000Z"
            }
        }
    ],
    "pagination": {
        "total": 1,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/construction`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `name` |  optional  | Search by Construction name.
    `city_id` |  optional  | Search by City.
    `status` |  optional  | Search by Construction status.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
    `service_id` |  optional  | Search by Construction name.
    `service_type_id` |  optional  | Search by Construction name.

<!-- END_92ef7348c3d91e762dad2d149050df98 -->

<!-- START_f20dc451262f1101d37b5236edf064f7 -->
## Save a new Construction to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/construction" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"voluptas","period":"in","address":"voluptas","service_id":6,"service_type_id":18,"city_id":19,"status":14,"staffs":[],"representative":"libero","representative_tel":"aperiam","representative_mail":"quia"}'

```

```javascript
const url = new URL(
    "http://localhost/api/construction"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "voluptas",
    "period": "in",
    "address": "voluptas",
    "service_id": 6,
    "service_type_id": 18,
    "city_id": 19,
    "status": 14,
    "staffs": [],
    "representative": "libero",
    "representative_tel": "aperiam",
    "representative_mail": "quia"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "Bảo trì thang máy",
        "service_type_id": "1",
        "address": "124 Thổ Quan, Đống Đa , HN",
        "service_id": "2",
        "person_in_charge": "Mai Thi Lại",
        "representative": "Trịnh Văn Quyết",
        "representative_mail": "flc@gmail.com",
        "representative_tel": "+84144000222",
        "updated_at": "2022-01-27T07:55:32.000000Z",
        "created_at": "2022-01-27T07:55:32.000000Z",
        "id": 4
    }
}
```

### HTTP Request
`POST api/construction`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Construction name.
        `period` | string |  optional  | Construction period (ky bao tri).
        `address` | string |  required  | Construction address.
        `service_id` | integer |  required  | Service's id.
        `service_type_id` | integer |  required  | Service type's id.
        `city_id` | integer |  optional  | City's id.
        `status` | integer |  optional  | Construction's status.
        `staffs` | array |  optional  | Array id cac' nhân viên phụ trách.
        `representative` | string |  required  | Representative's name.
        `representative_tel` | string |  required  | Representative's tel.
        `representative_mail` | string |  required  | Representative's mail.
    
<!-- END_f20dc451262f1101d37b5236edf064f7 -->

<!-- START_9dda3ec75a544e2bfc797c309a06ef9f -->
## Display the specified Construction

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/construction/qui" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/construction/qui"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 9,
        "name": "YUTO",
        "period": null,
        "address": "Lô K13 khu công nghiệp Quế Võ, Bắc Ninh",
        "service_id": 16,
        "service_type_id": 1,
        "status": 1,
        "person_in_charge": "Bùi Thanh Bình",
        "representative_id": null,
        "city_id": 69,
        "representative": "Phạm Đúc Hải",
        "representative_tel": "0968095072",
        "representative_mail": "hai.phamduc@hiconmne.vn",
        "created_by": null,
        "updated_by": null,
        "created_at": "2022-04-19T20:05:45.000000Z",
        "updated_at": "2022-04-19T20:05:45.000000Z",
        "staffs": [],
        "service": {
            "id": 16,
            "name": "Cải tạo nhà xưởng",
            "type_id": 1,
            "is_active": 1,
            "created_by": null,
            "updated_by": null,
            "created_at": "2022-04-19T20:03:56.000000Z",
            "updated_at": "2022-04-19T20:03:56.000000Z"
        },
        "service_type": {
            "id": 1,
            "name": "Bảo trì",
            "is_active": 1,
            "created_by": "admin",
            "updated_by": "admin",
            "created_at": "2022-01-27T09:33:38.000000Z",
            "updated_at": "2022-01-27T09:33:38.000000Z"
        }
    }
}
```

### HTTP Request
`GET api/construction/{construction}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `construction` |  required  | The ID of Construction.

<!-- END_9dda3ec75a544e2bfc797c309a06ef9f -->

<!-- START_e884d95fbb621226893a00039f1a95c6 -->
## Update the specified Construction in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/construction/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"at","period":"ipsum","address":"sapiente","service_id":5,"city_id":17,"status":15,"staffs":[],"representative":"facere","representative_tel":"reiciendis","representative_mail":"eaque","service_type_id":15}'

```

```javascript
const url = new URL(
    "http://localhost/api/construction/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "at",
    "period": "ipsum",
    "address": "sapiente",
    "service_id": 5,
    "city_id": 17,
    "status": 15,
    "staffs": [],
    "representative": "facere",
    "representative_tel": "reiciendis",
    "representative_mail": "eaque",
    "service_type_id": 15
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`PUT api/construction/{construction}`

`PATCH api/construction/{construction}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of updating Construction.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | Construction name.
        `period` | string |  optional  | Construction period (ky bao tri).
        `address` | string |  optional  | Construction address.
        `service_id` | integer |  optional  | Service's id.
        `city_id` | integer |  optional  | City's id.
        `status` | integer |  optional  | Construction's status.
        `staffs` | array |  optional  | Array id cac' nhân viên phụ trách.
        `representative` | string |  required  | Representative's name.
        `representative_tel` | string |  required  | Representative's tel.
        `representative_mail` | string |  required  | Representative's mail.
        `service_type_id` | integer |  optional  | Service type's id.
    
<!-- END_e884d95fbb621226893a00039f1a95c6 -->

<!-- START_b9f30a7a82b5fe40c03951848000050c -->
## Remove the specified Construction from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/construction/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/construction/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`DELETE api/construction/{construction}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of removing Construction.

<!-- END_b9f30a7a82b5fe40c03951848000050c -->

#ConstructionStatus Management


<!-- START_8efbe258b403f46396ec1dff3d1e5620 -->
## Display a list of Construction Status.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/status" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/status"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Hoàn thành",
            "description": "Done",
            "created_at": "2022-04-18T22:54:05.000000Z",
            "updated_at": null
        },
        {
            "id": 2,
            "name": "Dang thi công",
            "description": "Doing",
            "created_at": "2022-04-18T15:50:13.000000Z",
            "updated_at": "2022-04-18T15:50:13.000000Z"
        }
    ]
}
```

### HTTP Request
`GET api/status`


<!-- END_8efbe258b403f46396ec1dff3d1e5620 -->

<!-- START_7ea19774b653ab501f5f28825297b33d -->
## Save a new Construction Status to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/status" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"et","description":"alias"}'

```

```javascript
const url = new URL(
    "http://localhost/api/status"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "et",
    "description": "alias"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "Hoàn thành",
        "description": "hoàn thành",
        "updated_at": "2022-04-18T15:50:13.000000Z",
        "created_at": "2022-04-18T15:50:13.000000Z",
        "id": 2
    }
}
```

### HTTP Request
`POST api/status`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Status name.
        `description` | string |  optional  | Status description.
    
<!-- END_7ea19774b653ab501f5f28825297b33d -->

#Permission Management


<!-- START_ed8ced07a2186d44fa31e6f39b573d1c -->
## Display a list of permissions.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/permission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/permission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "title": "Super Admin",
            "key": "*",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Super Admin",
            "group_id": 0,
            "ability_key": "*"
        },
        {
            "id": 2,
            "title": "Tạo tài khoản mới",
            "key": "user-create",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "user-create"
        },
        {
            "id": 3,
            "title": "Reset mật khẩu",
            "key": "password-reset",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "password-reset"
        },
        {
            "id": 4,
            "title": "Sửa tài khoản",
            "key": "user-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "user-update"
        },
        {
            "id": 5,
            "title": "Tạo nhóm người dùng",
            "key": "role-create",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "role-create"
        },
        {
            "id": 6,
            "title": "Sửa nhóm người dùng",
            "key": "role-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "role-update"
        },
        {
            "id": 7,
            "title": "Tạo dịch vụ mới",
            "key": "service-create",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Quản trị dịch vụ",
            "group_id": 2,
            "ability_key": "service-create"
        },
        {
            "id": 8,
            "title": "Sửa dịch vụ",
            "key": "service-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Quản trị dịch vụ",
            "group_id": 2,
            "ability_key": "service-update"
        },
        {
            "id": 9,
            "title": "Tạo công trình",
            "key": "construction-create",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "construction-create"
        },
        {
            "id": 10,
            "title": "Sửa công trình",
            "key": "construction-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "construction-update"
        },
        {
            "id": 11,
            "title": "Thêm lịch sử dịch vụ",
            "key": "service-history-add",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "service-history-add"
        },
        {
            "id": 12,
            "title": "Cập nhật trạng thái",
            "key": "status-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "status-update"
        },
        {
            "id": 13,
            "title": "Báo cáo công trình",
            "key": "construction-report",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "construction-report"
        }
    ]
}
```

### HTTP Request
`GET api/permission`


<!-- END_ed8ced07a2186d44fa31e6f39b573d1c -->

#Report Management


<!-- START_ada5d5aec4765e54e2411f1c9c4f2d02 -->
## Display a listing of Reports.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/report?sort=facere&page=4&limit=9&order=voluptatibus&construction_id=illum&name=nobis&code=et&time_from=maxime&time_to=mollitia" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/report"
);

let params = {
    "sort": "facere",
    "page": "4",
    "limit": "9",
    "order": "voluptatibus",
    "construction_id": "illum",
    "name": "nobis",
    "code": "et",
    "time_from": "maxime",
    "time_to": "mollitia",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 35,
            "name": "test",
            "code": "323eaAAF",
            "publish_day": "12\/12\/2022",
            "publish_time": null,
            "representative_name": "Van Thanh",
            "address": "tes tsets",
            "construction_id": 4,
            "frequency": 1,
            "valid_date": null,
            "hicon_comment": "test",
            "customer_comment": "test",
            "created_at": "2022-06-09T09:26:12.000000Z",
            "updated_at": "2022-06-09T09:26:12.000000Z",
            "task": [
                {
                    "id": 27,
                    "name": "Tu chua chay",
                    "photo": "",
                    "description": "test",
                    "created_at": "2022-06-09T09:26:12.000000Z",
                    "updated_at": "2022-06-09T09:26:12.000000Z",
                    "report_id": 35,
                    "report_task": [
                        {
                            "id": 23,
                            "report_id": 35,
                            "task_id": 27,
                            "created_at": "2022-06-09T09:26:13.000000Z",
                            "updated_at": "2022-06-09T09:26:13.000000Z",
                            "photo": "images\/8l2GoiXnYMAOuaZZTJo2JZgwHiyrlqr2arc0aEL5.jpg"
                        },
                        {
                            "id": 24,
                            "report_id": 35,
                            "task_id": 27,
                            "created_at": "2022-06-09T09:26:13.000000Z",
                            "updated_at": "2022-06-09T09:26:13.000000Z",
                            "photo": "images\/RqrExtVZp5kZloZvgURhJNxU1VioFXYAZd3KhHH4.jpg"
                        }
                    ]
                },
                {
                    "id": 28,
                    "name": "Cap dong",
                    "photo": "",
                    "description": "test",
                    "created_at": "2022-06-09T09:26:13.000000Z",
                    "updated_at": "2022-06-09T09:26:13.000000Z",
                    "report_id": 35,
                    "report_task": [
                        {
                            "id": 25,
                            "report_id": 35,
                            "task_id": 28,
                            "created_at": "2022-06-09T09:26:13.000000Z",
                            "updated_at": "2022-06-09T09:26:13.000000Z",
                            "photo": "images\/rC19FjptvyAR9Er8fysGPs82kfqCYFc35eizxPdW.jpg"
                        },
                        {
                            "id": 26,
                            "report_id": 35,
                            "task_id": 28,
                            "created_at": "2022-06-09T09:26:13.000000Z",
                            "updated_at": "2022-06-09T09:26:13.000000Z",
                            "photo": "images\/FtusUc1meK0eUDtA7RjbWj9oCmcTgJ54EQI3EDAQ.jpg"
                        }
                    ]
                }
            ],
            "item": [
                {
                    "id": 4,
                    "report_id": 35,
                    "item_name": "thiet bi",
                    "item_unit": "cai",
                    "item_quantity": 2,
                    "created_at": "2022-06-09T09:26:13.000000Z",
                    "updated_at": "2022-06-09T09:26:13.000000Z"
                }
            ],
            "construction": null
        },
        {
            "id": 34,
            "name": "test",
            "code": "323eaAAF",
            "publish_day": "12\/12\/2022",
            "publish_time": null,
            "representative_name": "Van Thanh",
            "address": "tes tsets",
            "construction_id": 4,
            "frequency": 1,
            "valid_date": null,
            "hicon_comment": "test",
            "customer_comment": "test",
            "created_at": "2022-06-09T09:16:32.000000Z",
            "updated_at": "2022-06-09T09:16:32.000000Z",
            "task": [
                {
                    "id": 25,
                    "name": "Tu chua chay",
                    "photo": "",
                    "description": "test",
                    "created_at": "2022-06-09T09:16:32.000000Z",
                    "updated_at": "2022-06-09T09:16:32.000000Z",
                    "report_id": 34,
                    "report_task": [
                        {
                            "id": 19,
                            "report_id": 34,
                            "task_id": 25,
                            "created_at": "2022-06-09T09:16:32.000000Z",
                            "updated_at": "2022-06-09T09:16:32.000000Z",
                            "photo": "images\/fxZLRpmWzsy6pRpYcIs1Z1CENWJNsNw8ZIFymc7U.jpg"
                        },
                        {
                            "id": 20,
                            "report_id": 34,
                            "task_id": 25,
                            "created_at": "2022-06-09T09:16:32.000000Z",
                            "updated_at": "2022-06-09T09:16:32.000000Z",
                            "photo": "images\/54li8jLk9r1YphKHeRaxzIKYaQLgfbtehB72MCdJ.jpg"
                        }
                    ]
                },
                {
                    "id": 26,
                    "name": "Cap dong",
                    "photo": "",
                    "description": "test",
                    "created_at": "2022-06-09T09:16:32.000000Z",
                    "updated_at": "2022-06-09T09:16:32.000000Z",
                    "report_id": 34,
                    "report_task": [
                        {
                            "id": 21,
                            "report_id": 34,
                            "task_id": 26,
                            "created_at": "2022-06-09T09:16:32.000000Z",
                            "updated_at": "2022-06-09T09:16:32.000000Z",
                            "photo": "images\/4G33sEMT5BMveyoMyvPet0IJoPwjBUr0Hqgy2qcX.jpg"
                        },
                        {
                            "id": 22,
                            "report_id": 34,
                            "task_id": 26,
                            "created_at": "2022-06-09T09:16:32.000000Z",
                            "updated_at": "2022-06-09T09:16:32.000000Z",
                            "photo": "images\/rBwdSW4IMRKddaKLtza4Hj9wRuwGUsO2xHMGqp6R.jpg"
                        }
                    ]
                }
            ],
            "item": [
                {
                    "id": 3,
                    "report_id": 34,
                    "item_name": "thiet bi",
                    "item_unit": "cai",
                    "item_quantity": 2,
                    "created_at": "2022-06-09T09:16:32.000000Z",
                    "updated_at": "2022-06-09T09:16:32.000000Z"
                }
            ],
            "construction": null
        }
    ],
    "pagination": {
        "total": 2,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/report`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `construction_id` |  optional  | Search by construction_id.
    `name` |  optional  | Search by Report title.
    `code` |  optional  | Search by Report code.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss

<!-- END_ada5d5aec4765e54e2411f1c9c4f2d02 -->

<!-- START_513d4e19011ae1f92bd8858b5eb059b2 -->
## Save a new Report to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/report" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"aliquam","code":"totam","publish_day":"aut","publish_time":"esse","representative_name":"magni","address":"ut","construction_id":6,"frequency":4,"valid_date":"sunt","tasks":{"i":{"name":"est","images":{"j":"blanditiis"},"description":"quod"}},"items":{"i":{"name":"minus","unit":"quis","quantity":6}},"hicon_comment":"impedit","customer_comment":"amet","status":11}'

```

```javascript
const url = new URL(
    "http://localhost/api/report"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "aliquam",
    "code": "totam",
    "publish_day": "aut",
    "publish_time": "esse",
    "representative_name": "magni",
    "address": "ut",
    "construction_id": 6,
    "frequency": 4,
    "valid_date": "sunt",
    "tasks": {
        "i": {
            "name": "est",
            "images": {
                "j": "blanditiis"
            },
            "description": "quod"
        }
    },
    "items": {
        "i": {
            "name": "minus",
            "unit": "quis",
            "quantity": 6
        }
    },
    "hicon_comment": "impedit",
    "customer_comment": "amet",
    "status": 11
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "BÁO CÁO BẢO TRÌ HỆ THỐNG PHÒNG CHÁY CHỮA CHÁY",
        "code": "KSBT-BC-01",
        "publish_day": "05-01-2016",
        "publish_time": "01",
        "representative_name": "Khu vực sản xuất Sông Hồng 3",
        "address": "Hai Hau, Nam Định",
        "construction_id": 1,
        "frequency": 1,
        "hicon_comment": null,
        "customer_comment": null,
        "updated_at": "2022-06-03T07:55:03.000000Z",
        "created_at": "2022-06-03T07:55:03.000000Z",
        "id": 9
    }
}
```

### HTTP Request
`POST api/report`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Tiêu đề báo cáo.
        `code` | string |  required  | Mã ban hành.
        `publish_day` | string |  required  | Ngày ban hành.
        `publish_time` | string |  required  | Lần ban hành.
        `representative_name` | string |  required  | Họ tên đại diện.
        `address` | string |  required  | Địa chỉ.
        `construction_id` | integer |  required  | ID dự án (công trình).
        `frequency` | integer |  required  | Tần suất bảo dưỡng ( 1 : Hàng tháng, 2: Hàng quý, 3: Nửa năm, 4: hằng năm).
        `valid_date` | string |  required  | Ngày kiểm tra
        `tasks[i][name]` | string |  optional  | Tên công việc
        `tasks[i][images][j]` | file |  optional  | Ảnh chụp
        `tasks[i][description]` | string |  required  | Mô tả công việc
        `items[i][name]` | string |  required  | Tên thiết bị
        `items[i][unit]` | string |  required  | Đơn vị
        `items[i][quantity]` | integer |  required  | Số lượng thiết bị
        `hicon_comment` | string |  required  | Nhận xét của Hicon
        `customer_comment` | string |  required  | Nhận xét của khách hàng
        `status` | integer |  required  | Trạng thái báo cáo
    
<!-- END_513d4e19011ae1f92bd8858b5eb059b2 -->

<!-- START_5b0af81a3f9e1887739d6af03d783cce -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/report/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/report/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 35,
        "name": "test",
        "code": "323eaAAF",
        "publish_day": "12\/12\/2022",
        "publish_time": null,
        "representative_name": "Van Thanh",
        "address": "tes tsets",
        "construction_id": 4,
        "frequency": 1,
        "valid_date": null,
        "hicon_comment": "test",
        "customer_comment": "test",
        "created_at": "2022-06-09T09:26:12.000000Z",
        "updated_at": "2022-06-09T09:26:12.000000Z",
        "task": [
            {
                "id": 27,
                "name": "Tu chua chay",
                "photo": "",
                "description": "test",
                "created_at": "2022-06-09T09:26:12.000000Z",
                "updated_at": "2022-06-09T09:26:12.000000Z",
                "report_id": 35,
                "report_task": [
                    {
                        "id": 23,
                        "report_id": 35,
                        "task_id": 27,
                        "created_at": "2022-06-09T09:26:13.000000Z",
                        "updated_at": "2022-06-09T09:26:13.000000Z",
                        "photo": "images\/8l2GoiXnYMAOuaZZTJo2JZgwHiyrlqr2arc0aEL5.jpg"
                    },
                    {
                        "id": 24,
                        "report_id": 35,
                        "task_id": 27,
                        "created_at": "2022-06-09T09:26:13.000000Z",
                        "updated_at": "2022-06-09T09:26:13.000000Z",
                        "photo": "images\/RqrExtVZp5kZloZvgURhJNxU1VioFXYAZd3KhHH4.jpg"
                    }
                ]
            },
            {
                "id": 28,
                "name": "Cap dong",
                "photo": "",
                "description": "test",
                "created_at": "2022-06-09T09:26:13.000000Z",
                "updated_at": "2022-06-09T09:26:13.000000Z",
                "report_id": 35,
                "report_task": [
                    {
                        "id": 25,
                        "report_id": 35,
                        "task_id": 28,
                        "created_at": "2022-06-09T09:26:13.000000Z",
                        "updated_at": "2022-06-09T09:26:13.000000Z",
                        "photo": "images\/rC19FjptvyAR9Er8fysGPs82kfqCYFc35eizxPdW.jpg"
                    },
                    {
                        "id": 26,
                        "report_id": 35,
                        "task_id": 28,
                        "created_at": "2022-06-09T09:26:13.000000Z",
                        "updated_at": "2022-06-09T09:26:13.000000Z",
                        "photo": "images\/FtusUc1meK0eUDtA7RjbWj9oCmcTgJ54EQI3EDAQ.jpg"
                    }
                ]
            }
        ],
        "item": [
            {
                "id": 4,
                "report_id": 35,
                "item_name": "thiet bi",
                "item_unit": "cai",
                "item_quantity": 2,
                "created_at": "2022-06-09T09:26:13.000000Z",
                "updated_at": "2022-06-09T09:26:13.000000Z"
            }
        ],
        "construction": null
    }
}
```

### HTTP Request
`GET api/report/{report}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `task` |  required  | The ID of updating product.

<!-- END_5b0af81a3f9e1887739d6af03d783cce -->

<!-- START_2e51b8cbc2297029d21d33e21df978bf -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/report/non" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/report/non"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`DELETE api/report/{report}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `report` |  required  | The ID of removing report.

<!-- END_2e51b8cbc2297029d21d33e21df978bf -->

<!-- START_c6be500ff75557847b0cf9aeebdb50d2 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/report/remove/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/report/remove/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`POST api/report/remove/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `report` |  required  | The ID of removing report.

<!-- END_c6be500ff75557847b0cf9aeebdb50d2 -->

#Role Management


<!-- START_01fc43a11672802a440a34de5e43c9ec -->
## Display a list of roles.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/role" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/role"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "title": "Admin",
            "created_at": "2022-01-14T16:30:03.000000Z",
            "updated_at": "2022-01-14T16:30:07.000000Z",
            "description": "Full quyền"
        },
        {
            "id": 2,
            "title": "Quản trị",
            "created_at": "2022-01-14T16:30:05.000000Z",
            "updated_at": null,
            "description": ""
        }
    ]
}
```

### HTTP Request
`GET api/role`


<!-- END_01fc43a11672802a440a34de5e43c9ec -->

<!-- START_9da1b300a2c60ef9fb7d7bbbb9f7c300 -->
## Save a new Role to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/role" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"title":"at","description":"aperiam","permissions":[]}'

```

```javascript
const url = new URL(
    "http://localhost/api/role"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "title": "at",
    "description": "aperiam",
    "permissions": []
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "title": "HR",
        "description": "human resource",
        "updated_at": "2022-02-18T07:59:37.000000Z",
        "created_at": "2022-02-18T07:59:37.000000Z",
        "id": 5,
        "permissions": [
            {
                "id": 5,
                "role_id": 5,
                "permission_id": 3,
                "created_by": "admin",
                "created_at": "2022-02-18 07:59:37",
                "updated_at": null
            },
            {
                "id": 6,
                "role_id": 5,
                "permission_id": 4,
                "created_by": "admin",
                "created_at": "2022-02-18 07:59:37",
                "updated_at": null
            },
            {
                "id": 7,
                "role_id": 5,
                "permission_id": 5,
                "created_by": "admin",
                "created_at": "2022-02-18 07:59:37",
                "updated_at": null
            }
        ]
    }
}
```

### HTTP Request
`POST api/role`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  required  | Role name.
        `description` | string |  required  | Role description.
        `permissions` | array |  required  | List of permissions ids.
    
<!-- END_9da1b300a2c60ef9fb7d7bbbb9f7c300 -->

<!-- START_36f2eed567a95be3b454a71d1c5a4b97 -->
## Display the specified Role

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/role/doloribus" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/role/doloribus"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 13,
        "title": "Kế toán Tổng",
        "created_at": "2022-02-10T10:22:46.000000Z",
        "updated_at": "2022-02-10T10:22:46.000000Z",
        "description": "Trưởng phòng kế toán",
        "permissions": [
            5,
            7,
            6
        ]
    }
}
```

### HTTP Request
`GET api/role/{role}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `role` |  required  | The ID of Role.

<!-- END_36f2eed567a95be3b454a71d1c5a4b97 -->

<!-- START_82f3bd841b4e9f9e752a55da1338ab0c -->
## Update the specified Role in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/role/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"title":"omnis","description":"reiciendis","permissions":[]}'

```

```javascript
const url = new URL(
    "http://localhost/api/role/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "title": "omnis",
    "description": "reiciendis",
    "permissions": []
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`PUT api/role/{role}`

`PATCH api/role/{role}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of updating Role.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  required  | Role name.
        `description` | string |  required  | Role description.
        `permissions` | array |  required  | List of permissions ids.
    
<!-- END_82f3bd841b4e9f9e752a55da1338ab0c -->

#Service Management


<!-- START_970efa87e172ba755798eed88226812b -->
## Display a list of Services.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/service?sort=eum&page=11&limit=18&order=exercitationem&name=rerum&time_from=cumque&time_to=aperiam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/service"
);

let params = {
    "sort": "eum",
    "page": "11",
    "limit": "18",
    "order": "exercitationem",
    "name": "rerum",
    "time_from": "cumque",
    "time_to": "aperiam",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 3,
            "name": "Cải tạo nhà máy",
            "type_id": 1,
            "is_active": 1,
            "created_by": "admin",
            "updated_by": "admin",
            "created_at": "2022-01-26T14:41:26.000000Z",
            "updated_at": "2022-01-26T14:41:26.000000Z",
            "construction_count": 2,
            "service_type": {
                "id": 1,
                "name": "Bảo trì",
                "is_active": 1,
                "created_by": "admin",
                "updated_by": "admin",
                "created_at": "2022-01-26T21:33:38.000000Z",
                "updated_at": "2022-01-26T21:33:38.000000Z"
            }
        },
        {
            "id": 2,
            "name": "Thang máy",
            "type_id": 2,
            "is_active": 1,
            "created_by": "admin",
            "updated_by": null,
            "created_at": "2022-01-26T14:40:26.000000Z",
            "updated_at": "2022-01-26T14:40:26.000000Z",
            "construction_count": 0,
            "service_type": {
                "id": 2,
                "name": "Sửa chữa",
                "is_active": 1,
                "created_by": "admin",
                "updated_by": "admin",
                "created_at": "2022-01-26T21:33:38.000000Z",
                "updated_at": "2022-01-26T21:33:38.000000Z"
            }
        },
        {
            "id": 1,
            "name": "getall",
            "type_id": 1,
            "is_active": 1,
            "created_by": null,
            "updated_by": null,
            "created_at": "2022-01-26T14:39:24.000000Z",
            "updated_at": "2022-01-26T14:39:24.000000Z",
            "construction_count": 1,
            "service_type": {
                "id": 1,
                "name": "Bảo trì",
                "is_active": 1,
                "created_by": "admin",
                "updated_by": "admin",
                "created_at": "2022-01-26T21:33:38.000000Z",
                "updated_at": "2022-01-26T21:33:38.000000Z"
            }
        }
    ],
    "pagination": {
        "total": 3,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/service`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `name` |  optional  | Search by Service name.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss

<!-- END_970efa87e172ba755798eed88226812b -->

<!-- START_5447fe4970a6e01c8cf38eb6464cceef -->
## Save a new Service to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"voluptatum","type_id":19}'

```

```javascript
const url = new URL(
    "http://localhost/api/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "voluptatum",
    "type_id": 19
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "Repair and Replace",
        "type_id": "2",
        "updated_at": "2022-01-11T06:31:00.000000Z",
        "created_at": "2022-01-11T06:31:00.000000Z",
        "id": 2
    }
}
```

### HTTP Request
`POST api/service`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Service name.
        `type_id` | integer |  required  | Service type's id.
    
<!-- END_5447fe4970a6e01c8cf38eb6464cceef -->

<!-- START_0069a867ee92ce010687a63e6e686cac -->
## Display the specified Service

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/service/dolor" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/service/dolor"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Repost",
        "type_id": 1,
        "is_active": 1,
        "created_by": "admin",
        "updated_by": null,
        "created_at": "2022-01-11T04:22:16.000000Z",
        "updated_at": "2022-01-11T04:22:16.000000Z",
        "service_type": {
            "id": 1,
            "name": "Bảo trì",
            "is_active": 1,
            "created_by": "admin",
            "updated_by": null,
            "created_at": "2022-01-11T14:18:48.000000Z",
            "updated_at": "2022-01-11T14:18:50.000000Z"
        }
    }
}
```

### HTTP Request
`GET api/service/{service}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of Service.

<!-- END_0069a867ee92ce010687a63e6e686cac -->

<!-- START_d3cdddf820be263f473e0b512d8d02f8 -->
## Update the specified Service in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/service/vel" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"omnis","type_id":11,"is_active":12}'

```

```javascript
const url = new URL(
    "http://localhost/api/service/vel"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "omnis",
    "type_id": 11,
    "is_active": 12
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`PUT api/service/{service}`

`PATCH api/service/{service}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of updating Service.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Service name.
        `type_id` | integer |  required  | Service type's id.
        `is_active` | integer |  required  | enum(1,0)
    
<!-- END_d3cdddf820be263f473e0b512d8d02f8 -->

<!-- START_9f5d6d2730461434dc8a14091d3912de -->
## Remove the specified Service from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/service/magni" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/service/magni"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`DELETE api/service/{service}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of removing Service.

<!-- END_9f5d6d2730461434dc8a14091d3912de -->

#ServiceType Management


<!-- START_0b91b7510ce4774448923055a4a34097 -->
## Display a list of ServiceTypes.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/service-type" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/service-type"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Bảo trì",
            "is_active": 1,
            "created_by": "admin",
            "updated_by": "admin",
            "created_at": "2022-01-26T21:33:38.000000Z",
            "updated_at": "2022-01-26T21:33:38.000000Z"
        },
        {
            "id": 2,
            "name": "Sửa chữa",
            "is_active": 1,
            "created_by": "admin",
            "updated_by": "admin",
            "created_at": "2022-01-26T21:33:38.000000Z",
            "updated_at": "2022-01-26T21:33:38.000000Z"
        }
    ]
}
```

### HTTP Request
`GET api/service-type`


<!-- END_0b91b7510ce4774448923055a4a34097 -->

#Task Management


<!-- START_96431051513da723c0741b326fe7719c -->
## Display a listing of Tasks.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/task?sort=aperiam&page=17&limit=2&order=corrupti&name=accusamus&time_from=omnis&time_to=at" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/task"
);

let params = {
    "sort": "aperiam",
    "page": "17",
    "limit": "2",
    "order": "corrupti",
    "name": "accusamus",
    "time_from": "omnis",
    "time_to": "at",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Tủ trung tâm báo cháy",
            "photo": "images\/QzJu1bF2ZLnyLBXcho8wli54KO3HCs9ntCJ42Tn7.jpg",
            "description": "• Kiểm tra chức năng tủ trung tâm, mức độ an toàn tương thích của hệ thống \r\n    • Kiểm tra tín hiệu thông số kỹ thuật bo mạch của trung tâm báo cháy\r\n    • Kiểm tra, đo kẹp chỉ số A, V khi hệ thống hoạt động \r\n    • Vệ sinh tủ trung tâm báo cháy",
            "created_at": "2022-06-02T06:25:30.000000Z",
            "updated_at": "2022-06-02T06:25:30.000000Z"
        }
    ],
    "pagination": {
        "total": 1,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/task`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `name` |  optional  | Search by Task title.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss

<!-- END_96431051513da723c0741b326fe7719c -->

<!-- START_bb92a432ca8bb8f24cd8a0baf9beee3f -->
## Save a new Task to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/task" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"enim","photo":"maiores","description":"aliquid"}'

```

```javascript
const url = new URL(
    "http://localhost/api/task"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "enim",
    "photo": "maiores",
    "description": "aliquid"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "Tủ trung tâm báo cháy",
        "photo": "images\/QzJu1bF2ZLnyLBXcho8wli54KO3HCs9ntCJ42Tn7.jpg",
        "description": "• Kiểm tra chức năng tủ trung tâm, mức độ an toàn tương thích của hệ thống \r\n    • Kiểm tra tín hiệu thông số kỹ thuật bo mạch của trung tâm báo cháy\r\n    • Kiểm tra, đo kẹp chỉ số A, V khi hệ thống hoạt động \r\n    • Vệ sinh tủ trung tâm báo cháy",
        "updated_at": "2022-06-02T06:25:30.000000Z",
        "created_at": "2022-06-02T06:25:30.000000Z",
        "id": 1
    }
}
```

### HTTP Request
`POST api/task`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Task title.
        `photo` | file |  optional  | Photo
        `description` | Description |  optional  | 
    
<!-- END_bb92a432ca8bb8f24cd8a0baf9beee3f -->

<!-- START_3cdd4bb98e8b6cf888d6fab5c7e8c9a3 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/task/culpa" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/task/culpa"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Tủ trung tâm báo cháy",
        "photo": "images\/QzJu1bF2ZLnyLBXcho8wli54KO3HCs9ntCJ42Tn7.jpg",
        "description": "• Kiểm tra chức năng tủ trung tâm, mức độ an toàn tương thích của hệ thống \r\n    • Kiểm tra tín hiệu thông số kỹ thuật bo mạch của trung tâm báo cháy\r\n    • Kiểm tra, đo kẹp chỉ số A, V khi hệ thống hoạt động \r\n    • Vệ sinh tủ trung tâm báo cháy",
        "created_at": "2022-06-02T06:25:30.000000Z",
        "updated_at": "2022-06-02T06:25:30.000000Z"
    }
}
```

### HTTP Request
`GET api/task/{task}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `task` |  required  | The ID of updating product.

<!-- END_3cdd4bb98e8b6cf888d6fab5c7e8c9a3 -->

<!-- START_c403207afbb75b64b3a95ad9d0b713fd -->
## Update the specified Task in storage.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/task/rerum/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"nesciunt","photo":"omnis","description":"ut"}'

```

```javascript
const url = new URL(
    "http://localhost/api/task/rerum/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "nesciunt",
    "photo": "omnis",
    "description": "ut"
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`GET api/task/{task}/edit`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `task` |  required  | The ID of updating task.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Task title.
        `photo` | file |  optional  | Photo
        `description` | Description |  optional  | 
    
<!-- END_c403207afbb75b64b3a95ad9d0b713fd -->

<!-- START_e0e4622abf1eab33b7a3c1bb1deb261d -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/task/ipsum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/task/ipsum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`DELETE api/task/{task}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `task` |  required  | The ID of removing task.

<!-- END_e0e4622abf1eab33b7a3c1bb1deb261d -->

<!-- START_e81e0c34e8f7e0a26a92f1162bed912c -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/task/show/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/task/show/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Tủ trung tâm báo cháy",
        "photo": "images\/QzJu1bF2ZLnyLBXcho8wli54KO3HCs9ntCJ42Tn7.jpg",
        "description": "• Kiểm tra chức năng tủ trung tâm, mức độ an toàn tương thích của hệ thống \r\n    • Kiểm tra tín hiệu thông số kỹ thuật bo mạch của trung tâm báo cháy\r\n    • Kiểm tra, đo kẹp chỉ số A, V khi hệ thống hoạt động \r\n    • Vệ sinh tủ trung tâm báo cháy",
        "created_at": "2022-06-02T06:25:30.000000Z",
        "updated_at": "2022-06-02T06:25:30.000000Z"
    }
}
```

### HTTP Request
`GET api/task/show/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `task` |  required  | The ID of updating product.

<!-- END_e81e0c34e8f7e0a26a92f1162bed912c -->

<!-- START_81a96ef3f00f95c944195809c64a9fab -->
## Update the specified Task in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/task/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"qui","photo":"suscipit","description":"quae"}'

```

```javascript
const url = new URL(
    "http://localhost/api/task/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "qui",
    "photo": "suscipit",
    "description": "quae"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`POST api/task/edit/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `task` |  required  | The ID of updating task.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Task title.
        `photo` | file |  optional  | Photo
        `description` | Description |  optional  | 
    
<!-- END_81a96ef3f00f95c944195809c64a9fab -->

<!-- START_2930f7c2f0c0e05836086e3961815c7e -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/task/remove/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/task/remove/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`POST api/task/remove/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `task` |  required  | The ID of removing task.

<!-- END_2930f7c2f0c0e05836086e3961815c7e -->

#User Management


<!-- START_2b6e5a4b188cb183c7e59558cce36cb6 -->
## Display a listing of Users.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user?sort=et&page=1&limit=16&order=sint&name=ullam&username=et&email=vel&tel=16&time_from=occaecati&time_to=dolores" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user"
);

let params = {
    "sort": "et",
    "page": "1",
    "limit": "16",
    "order": "sint",
    "name": "ullam",
    "username": "et",
    "email": "vel",
    "tel": "16",
    "time_from": "occaecati",
    "time_to": "dolores",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 8,
            "name": "admin1244a",
            "email": "minmid@gmail.com",
            "email_verified_at": null,
            "created_at": "2022-02-09T09:36:10.000000Z",
            "updated_at": "2022-02-09T09:36:10.000000Z",
            "username": "admin1",
            "tel": "0978442111",
            "role_id": 1,
            "status": 1
        },
        {
            "id": 1,
            "name": "HieuNguyen",
            "email": "ndhieu@gmail.com",
            "email_verified_at": null,
            "created_at": "2022-02-09T15:46:33.000000Z",
            "updated_at": null,
            "username": "hieund",
            "tel": "0326778888",
            "role_id": 1,
            "status": 1
        }
    ],
    "pagination": {
        "total": 2,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/user`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `name` |  optional  | Search by user's name.
    `username` |  optional  | Search by user's username.
    `email` |  optional  | Search by user's email.
    `tel` |  optional  | Search by user's telephone number.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss

<!-- END_2b6e5a4b188cb183c7e59558cce36cb6 -->

<!-- START_f0654d3f2fc63c11f5723f233cc53c83 -->
## Save a new User to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"laborum","username":"unde","password":"maiores","email":"occaecati","tel":"molestiae","role_id":6}'

```

```javascript
const url = new URL(
    "http://localhost/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "laborum",
    "username": "unde",
    "password": "maiores",
    "email": "occaecati",
    "tel": "molestiae",
    "role_id": 6
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "admin1244a",
        "username": "admin1",
        "email": "minmid@gmail.com",
        "tel": "0978442111",
        "role_id": "1",
        "updated_at": "2022-02-09T09:36:10.000000Z",
        "created_at": "2022-02-09T09:36:10.000000Z",
        "id": 8
    }
}
```

### HTTP Request
`POST api/user`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | User's name.
        `username` | string |  required  | User's username.
        `password` | string |  required  | User's username.
        `email` | string |  required  | User's email.
        `tel` | string |  required  | User's tel number.
        `role_id` | integer |  required  | User's role id.
    
<!-- END_f0654d3f2fc63c11f5723f233cc53c83 -->

<!-- START_ceec0e0b1d13d731ad96603d26bccc2f -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user/quam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user/quam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Hieu Nguyen",
        "email": "ndhieu@gmail.com",
        "email_verified_at": null,
        "created_at": "2022-02-09T15:46:33.000000Z",
        "updated_at": null,
        "username": "hieund",
        "tel": "0326778888",
        "role_id": 1,
        "status": 1
    }
}
```

### HTTP Request
`GET api/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `user` |  required  | The ID of user.

<!-- END_ceec0e0b1d13d731ad96603d26bccc2f -->

<!-- START_a4a2abed1e8e8cad5e6a3282812fe3f3 -->
## Update the specified User in database.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/user/sed" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"quas","username":"ut","password":"consectetur","email":"qui","tel":"voluptatibus","role_id":3}'

```

```javascript
const url = new URL(
    "http://localhost/api/user/sed"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "quas",
    "username": "ut",
    "password": "consectetur",
    "email": "qui",
    "tel": "voluptatibus",
    "role_id": 3
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`PUT api/user/{user}`

`PATCH api/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `user` |  required  | The ID of updating User.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | User's name.
        `username` | string |  optional  | User's username.
        `password` | string |  optional  | User's username.
        `email` | string |  optional  | User's email.
        `tel` | string |  optional  | User's tel number.
        `role_id` | integer |  optional  | User's role id.
    
<!-- END_a4a2abed1e8e8cad5e6a3282812fe3f3 -->

<!-- START_4bb7fb4a7501d3cb1ed21acfc3b205a9 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/user/nulla" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user/nulla"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`DELETE api/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `user` |  required  | The ID of updating user.

<!-- END_4bb7fb4a7501d3cb1ed21acfc3b205a9 -->

#general


<!-- START_4dfafe7f87ec132be3c8990dd1fa9078 -->
## Return an empty response simply to trigger the storage of the CSRF cookie in the browser.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/sanctum/csrf-cookie" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/sanctum/csrf-cookie"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`GET sanctum/csrf-cookie`


<!-- END_4dfafe7f87ec132be3c8990dd1fa9078 -->

<!-- START_bb1c3c6798ff2dd1e763f8489dc857c1 -->
## api/list
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "CsrfCookieController": [
        "show"
    ],
    "Controller": [
        "routeList",
        "login",
        "resetPassword",
        "loginAdmin"
    ],
    "ReportController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy",
        "destroy"
    ],
    "TaskController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy",
        "show",
        "edit",
        "destroy"
    ],
    "ServiceTypeController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "ServiceController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "ConstructionController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "UserController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "RoleController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "PermissionController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "ConstructionStatusController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "CityController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ]
}
```

### HTTP Request
`GET api/list`


<!-- END_bb1c3c6798ff2dd1e763f8489dc857c1 -->

<!-- START_3a975b26df4276161ca9841e2fb11374 -->
## api/admin-login
> Example request:

```bash
curl -X POST \
    "http://localhost/api/admin-login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/admin-login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/admin-login`


<!-- END_3a975b26df4276161ca9841e2fb11374 -->



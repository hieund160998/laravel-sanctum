<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Successfully send your message</title>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td width="640">
            <table border="0" cellpadding="0" cellspacing="0" width="640">
                <tbody>
                <tr>
                    <td width="640">
                        <div class="content-block">
                            <p><strong>Thông tin tài khoản:</strong></p>
                            <p><strong>Tên:</strong> {{ $data['full_name'] }} <br><strong>E-mail:</strong> {{ $data['email'] }}<br><strong>SĐT:</strong> {{ $data['phone'] }}<br><strong>Yêu Cầu:</strong> {{ $data['subject'] }}</p>
                            <p><strong>Mật khẩu mới:</strong><br> {{ $data['message'] }}</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="40" width="640"> </td>
                </tr>
                <tr>
                    <td>
                        <p align="center">Thank you, <em>omcloud.vn</em></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>


* Yêu cầu cài đặt: `composer`, `>= php7.3`, `xampp`, `mysql`

1. Clone project về thư mục chứa code
```  
git clone https://gitlab.com/hieund160998/laravel-sanctum.git
```

2. Tạo file config môi trường
```
cp .env.example .env
```

3. Install package.json (via `composer`)
```
composer install
```

4. Tạo key cho project Laravel
```
php artisan key:generate
```

5. Chạy migrate tạo bảng cho database
```
php artisan migrate
```

6. Chạy project (mặc định port 8000)
```
php artisan serve
```
